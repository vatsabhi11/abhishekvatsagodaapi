Feature: To test the Get Functionality of Various Quotes API

  Scenario Outline: To test the API functionality of quote api with different combinations of queryparam

    Given User is initializing the api with the "<Endpoint>"
    Then  User is adding the "<QueryParam1>","<QueryValue1>","<QueryParam2>","<QueryValue2>"
    And  User is making a get call
    Then user is validating the statuscode as 200
    And user is validating the content type of the response



    Examples:
      | Endpoint | QueryParam1 | QueryValue1              | QueryParam2 | QueryValue2 |
      | quotes   | tags        | famous-quotes\|wisdom    | author      | Confucius   |
      | quotes   | tags        | friendship               | page        | 2           |
      | quotes   | tags        | love\|happiness          |             |             |
      | quotes   | tags        | technology,famous-quotes |             |             |
      | quotes   | page        | 1                        |             |             |
      | quotes   | page        | 2                        |             |             |
      | quotes   | author      | albert-einstein          |             |             |