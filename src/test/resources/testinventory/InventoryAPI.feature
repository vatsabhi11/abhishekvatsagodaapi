Feature: To test the various features of Inventory API

  @Regression
  Scenario Outline: To test the success context of fetch inventory api

    Given user is initializing the inventory api with the "<Endpoint>"
    Then  user is adding the "<hotel_id>","<room_id>","<startdate>","<enddate>"
    And  user is making a get call for inventory api
    Then user is validating the statuscode as 200
    And user is validating the content type of the response
    And user is validating the scheme for the response received
    And user is validating the hotel_id and room_id values are exact match as mentioned in url


    Examples:
      | Endpoint        | hotel_id | room_id | startdate  | enddate    |
      | fetch/inventory | 1        | 101     | 2021-02-19 | 2021-02-21 |
      | fetch/inventory | 1        | 100     | 2021-02-20 | 2021-02-21 |

  @Regression
  Scenario Outline: To test the success context of update inventory api

    Given user is initializing the inventory api with the "<Endpoint>"
    Then  user is adding the "<hotel_id>","<room_id>","<startdate>","<enddate>"
    And  user is posting the new "<rates>" and "<rem_rooms>" info by making a post call
    Then user is validating the statuscode as 200
    And user is validating the content type of the response
    And user is validating the scheme for the response received
    And user is validating the rates and remaining rooms are reflecting as expected


    Examples:
      | Endpoint         | hotel_id | room_id | startdate  | enddate    | rates | rem_rooms |
      | update/inventory | 1        | 101     | 2021-02-19 | 2021-02-21 | 208   | 8         |
      | update/inventory | 1        | 100     | 2021-02-20 | 2021-02-21 | 210   | 5         |


  @Regression
  Scenario: To test the error context for fetch inventory api
    Given user is initializing the fetch inventory api
    And user is making a get call by using different content type
    Then user is validating status code as unsupported media type 415

  @Regression
  Scenario: To test the error context for update inventory api with wrong content type
    Given user is initializing the fetch inventory api
    And user is making a post call by using different content type
    Then user is validating status code as unsupported media type 415

  @Regression
  Scenario: To test the error context for update inventory api with an empty body
    Given user is initializing the fetch inventory api
    And user is making a post call with an empty body
    Then user is validating status code as bad request 400


  @Regression
  Scenario: To test the error context for update inventory api with an empty body and empty header
    Given user is initializing the fetch inventory api
    And user is making a post call with an empty body
    Then user is validating status code as forbidden 403


  @LoadTest
  Scenario Outline: To perform load test in inventory api with get,post ,update

    Given user is creating a thread group with 50 threads and 5 loop counts for different requests
    When user is initializing the inventory api with the "<Endpoint>"
    Then  user is adding the "<hotel_id>","<room_id>","<startdate>","<enddate>"
    And  user is making a get,post and update calls for inventory api
    Then user is validating the statuscode as 200
    And user is validating the content type of the response
    And user is validating the scheme for the response received

    Examples:
      | Endpoint  | hotel_id | room_id | startdate  | enddate    |
      | inventory | 1        | 101     | 2021-02-19 | 2021-02-21 |
      | inventory | 1        | 100     | 2021-02-20 | 2021-02-21 |


  @LoadTest
  Scenario Outline: To perform load test in update inventory api post and update calls

    Given user is creating a thread group with 30 threads and 4 loop counts for different requests
    When user is initializing the inventory api with the "<Endpoint>"
    Then  user is adding the "<hotel_id>","<room_id>","<startdate>","<enddate>"
    And  user is making a post and update calls for inventory api with "<rates>" and "<rem_rooms>"
    Then user is validating the statuscode as 200
    And user is validating the content type of the response
    And user is validating the scheme for the response received


    Examples:
      | Endpoint         | hotel_id | room_id | startdate  | enddate    | rates | rem_rooms |
      | update/inventory | 1        | 101     | 2021-02-19 | 2021-02-21 | 208   | 8         |
      | update/inventory | 1        | 100     | 2021-02-20 | 2021-02-21 | 210   | 5         |
