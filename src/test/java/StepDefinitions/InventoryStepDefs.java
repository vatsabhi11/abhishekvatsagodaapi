package StepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class InventoryStepDefs {


    @Given("user is initializing the inventory api with the {string}")
    public void userIsInitializingTheInventoryApiWithThe(String arg0) {
    }

    @Then("user is adding the {string},{string},{string},{string}")
    public void userIsAddingThe(String arg0, String arg1, String arg2, String arg3) {
    }

    @And("user is making a get call for inventory api")
    public void userIsMakingAGetCallForInventoryApi() {
    }

    @And("user is validating the scheme for the response received")
    public void userIsValidatingTheSchemeForTheResponseReceived() {
    }

    @And("user is validating the hotel_id and room_id values are exact match as mentioned in url")
    public void userIsValidatingTheHotel_idAndRoom_idValuesAreExactMatchAsMentionedInUrl() {
    }

    @And("user is posting the new <rates> and <rem_rooms> info by making a post call")
    public void userIsPostingTheNewRatesAndRem_roomsInfoByMakingAPostCall() {
    }

    @And("user is validating the rates and remaining rooms are reflecting as expected")
    public void userIsValidatingTheRatesAndRemainingRoomsAreReflectingAsExpected() {
    }

    @Given("user is creating a thread group with {int} threads and {int} loop counts for different requests")
    public void userIsCreatingAThreadGroupWithThreadsAndLoopCountsForDifferentRequests(int arg0, int arg1) {
    }

    @And("user is making a get,post and update calls for inventory api")
    public void userIsMakingAGetPostAndUpdateCallsForInventoryApi() {
    }





    @And("user is making a post and update calls for inventory api with {string} and {string}")
    public void userIsMakingAPostAndUpdateCallsForInventoryApiWithAnd(String arg0, String arg1) {
    }

    @And("user is posting the new {string} and {string} info by making a post call")
    public void userIsPostingTheNewAndInfoByMakingAPostCall(String arg0, String arg1) {
    }

    @Given("user is initializing the fetch inventory api")
    public void userIsInitializingTheFetchInventoryApi() {
    }

    @And("user is making a get call by using different content type")
    public void userIsMakingAGetCallByUsingDifferentContentType() {
    }

    @Then("user is validating status code as unsupported media type {int}")
    public void userIsValidatingStatusCodeAsUnsupportedMediaType(int arg0) {
    }

    @And("user is making a post call by using different content type")
    public void userIsMakingAPostCallByUsingDifferentContentType() {
    }

    @And("user is making a post call with an empty body")
    public void userIsMakingAPostCallWithAnEmptyBody() {
    }

    @Then("user is validating status code as bad request {int}")
    public void userIsValidatingStatusCodeAsBadRequest(int arg0) {
    }

    @Then("user is validating status code as forbidden {int}")
    public void userIsValidatingStatusCodeAsForbidden(int arg0) {
    }
}
