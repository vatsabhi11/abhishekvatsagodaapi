package StepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import utils.Utility;

import java.util.HashMap;


public class QuotesStepDefs{
    public String endpoint;
    public HashMap<String,String> queryparams = new HashMap<>();
    public Response response;
    public RequestSpecification request = RestAssured.given();
    Utility util = new Utility();

    @Given("User is initializing the api with the {string}")
    public void user_is_initializing_the_api_with_the(String string) {
        // user is setting up the base uri for the call
        request.baseUri(util.Property("BaseURI"));

        endpoint=string;
    }

    @Then("User is adding the {string},{string},{string},{string}")
    public void user_is_adding_the(String queryP1, String queryPValue1, String queryP2, String queryPValue2) {
        //setting up the query params for the api
        queryparams.put(queryP1,queryPValue1);
        queryparams.put(queryP2,queryPValue2);


    }

    @Then("User is making a get call")
    public void user_is_making_a_get_call() {

        // user is making a get call
        response = request.queryParams(queryparams).get(endpoint);

    }

    @Then("user is validating the statuscode as {int}")
    public void user_is_validating_the_statuscode_as(Integer int1) {

        // user is validating the statuscode
        Assert.assertTrue(int1.equals(response.getStatusCode()));

        
    }

    @Then("user is validating the content type of the response")
    public void user_is_validating_the_content_type_of_the_response() {
        // user is validating the content type from the response
        Assert.assertEquals("application/json; charset=utf-8",response.getContentType());
    }


}
