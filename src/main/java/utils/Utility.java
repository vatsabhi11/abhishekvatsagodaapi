package utils;

import java.io.*;
import java.util.Properties;

public class Utility {

    public Properties properties;


    public Utility(){


        if(!Config())
        {
            try{

                throw new Exception("Configuration FIle is not loaded successfully");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    }

    public boolean Config()
    {
        properties = new Properties();
        String configPath = System.getProperty("user.dir")+"\\Configuration\\Environment.properties";

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(configPath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + configPath);
        }
        return true;

    }

    public String Property(String propertyName){

        return properties.getProperty(propertyName);
    }
}
