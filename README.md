# README #

This README would normally document whatever steps are necessary to get your application up and running.


##Set up

clone the repository into Intellij IDEA 

##Excecution
Use TestRunner to execute the test and to generate the reports

### What is this repository for? ###
 AGODA API Automation

1. Inventory API Test Scenarios:
    These scenarios has been covered in InventoryAPI.Feature under testinventory directory
    path: src/test/resources/testinventory/InventoryAPI.feature
2. test-quotes-api : 
   Test scenarios has been mentioned in the feature file named QuotesAPI.feature,
   implementation for the same in available in QuotesStepsDefs: src/test/java/StepDefinitions/QuotesStepDefs.java
    path for feature file : src/test/resources/testquotes/QuotesAPI.feature

##Report
report for test execution has been present in report directory under the name Agoda.html.

##Jenkins

Jenkins reports snapshot and Jenkins dashboard snapshot has been shared under JenkinsSnapshot directory.

##Configuration 
This directory has been created to declare variables globally

##Tools 
API Automation : Rest Assured
Reporting: Extent Reports
Build Tool: Maven
CICD: Jenkins
BDD: Cucumber
IDE: Intellij IDEA
Testing Framework: Junit
